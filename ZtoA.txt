
Documentation for:  
		ZTOA.XREL.
Written by: 
		Joseph L Moschini
                jmos@iprimus.com.au

Usage:  
		First transfer Zrdos.ram to /rd.

		ZTOA.XREL is for transfering Zrdos files to Applix files.
		To use it eg.

				ztoa <CR>

				press space bar for next file.

				press Enter to transfer displayed file.

				press Esc to escape program.



Associated files:
			ATOZ.C and ZD.C are sorce files writen in
			HI TECH C.
 
	
Purpose:
			The purpose of these files is to get information
			from CPM for Applix and for Applix information
			to CPM.

Commands:
			?

The Rules:
			One rules is to have ZRDOS.RAM on /rd.

Notes:

	These programs originated from when I had given up my old
	Big Board 2 for the new Applix with Zrdos. So I had a
	problem on hand to transfer all my old programs to zrdos.
	This was accomplished by using Mark Harvey's Modem program
	on the Applix and the Smodem on the BB2 CPM, which took at least
	3 long nights. Next came the problem to put all the programs
	zrdos. This was accomplished again by using Zrdos.ram after 
	execution of zrdos. The programs access Zrdos.ram to get the
	infomation. One other way of doing this I discovered after
	making the these programs, was to make a transfer program
	inside Zrdos to access the files on the Applix. I don't
	know which way would be easy, because I haven't tried the other
	way yet.
	I found these program very useful so I dont have to type out
	C source codes again. Plus was handy to get Z80 code out to my
	Eprom programer.

		BUG FIXXXX.
	The ztoa file had a slight problem with CPM's extention files.
	Works on Version 4 ok now I don't know if it will work the past
	Versions.

